'use strict';
const puppeteer = require('puppeteer');

const data = require('./mockData/adminAdventureCompassResponse');
const {start} = require('./mainFunctions/start');

const init = async () => {
	global.browser = await puppeteer.launch();
	global.page = null;
	global.resultData = {};

	// запуск рекурсивной функции по ссылкам
	await go(data, 0);

	console.log('data', resultData);
};

const go = async (data, index) => {
	await start(data[index]);
	if (index < data.length - 1) return await go(data, index + 1);
};

init();