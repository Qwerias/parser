/**
 *
 * @param element
 * @returns {number} Возвращает рейтинг
 */
exports.getRating = async function(element)
{
	const classes = [
		'bubble_15', 'bubble_20', 'bubble_25', 'bubble_30', 'bubble_35', 'bubble_40', 'bubble_45', 'bubble_50'
	];
	const bubbleRating = await element.$('span.ui_bubble_rating');

	classes.forEach(className => {
		if (bubbleRating.getProperty('class') === className)
		{
			return className.match(/\d+/)[0] / 10;
		}
	});
};