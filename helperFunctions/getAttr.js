exports.getAttr = async function (element, attr) {
	return await page.evaluate(el => el.getAttribute(attr), element);
};