exports.ColoredInfo = class {
	static major(text)
	{
		console.log('\x1b[32m%s\x1b[0m', text);
	}

	static minor(text)
	{
		console.log('\x1b[36m%s\x1b[0m', text);
	}
};