/**
 *
 * @param element {HTMLElement}
 */
exports.click = async function(element) {
	return await page.evaluate(el => el.click(), element);
};