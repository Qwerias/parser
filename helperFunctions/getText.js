/**
 *
 * @param element {HTMLElement}
 */
exports.getText = async function (element) {
	return await page.evaluate(el => el.textContent, element);
};