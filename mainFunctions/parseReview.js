const {getText} = require('../helperFunctions/getText');
const {getAttr} = require('../helperFunctions/getAttr');
const {click} = require('../helperFunctions/click');
const {getRating} = require('../helperFunctions/getRating');
const {ColoredInfo} = require('../helperFunctions/ColoredInfo/ColoredInfo');

exports.parseReview = async (container, index) => {
	ColoredInfo.minor('Started parsing review: ' + index);

	const result = {
		title: null,
		short_body: null,
		body: null,
		rating: null,
		event_date: null,
		review_date: null,
		user_image: null,
		user_name: null,
		short_reply: null,
		reply: null,
		reply_date: null
	};

	result.title = await container.$eval('div.quote a.title span.noQuotes', el => el.textContent);

	/**
	 * Review
	 */

	const bodyBox = await container.$('div.prw_reviews_text_summary_hsx div.entry p.partial_entry');
	result.short_body = await getText(bodyBox);

	const button = await bodyBox.$('span.ulBlueLinks');

	if (button)
	{
		await click(button);
		result.body = await getText(bodyBox);
	}

	result.rating = await getRating(container);

	result.event_date = await container.$eval('div.prw_reviews_stay_date_hsx', el => el.textContent);
	result.review_date = await container.$eval('span.ratingDate', el => el ? el.getAttribute('title') : null);

	result.user_image = await container.$eval('div.ui_avatar img', el => el ? el.getAttribute('src') : null);
	result.user_name = await container.$eval('div.info_text div', el => el.textContent);

	/**
	 * Reply
	 */

	const replyBox = await container.$('div.mgrRspnInline div.prw_reviews_text_summary_hsx .entry p.partial_entry');
	if (replyBox)
	{
		result.short_reply = await getText(replyBox);

		const buttonMore = await replyBox.$('span.ulBlueLinks');
		if (buttonMore)
		{
			await click(buttonMore);
		}

		result.reply = await getText(replyBox);

		const replyDateElement = await container.$('div.responseDate');
		if (replyDateElement)
		{
			result.reply_date = await getAttr(replyDateElement, 'title');
		}
	}
	return result;
};