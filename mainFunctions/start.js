const {ColoredInfo} = require('../helperFunctions/ColoredInfo/ColoredInfo');

const {parseReview} = require('./parseReview');

/**
 *
 * @param data {{id: number, link: string}}
 * @returns {Promise<void>}
 */
exports.start = async data => {
	ColoredInfo.major('Started parsing link: ' + data.link);

	const reviewsSelector = '.review-container';

	page = await browser.newPage();
	await page.goto(data.link);

	// создаем свойство-массив для результата парсинга текущей ссылки с ключем id этой ссылки
	resultData[data.id] = [];

	const currentContainers = await page.$$(reviewsSelector);

	// запуск рекурсивной функции по контейнерам
	await parse(data, currentContainers, 0);
};

const parse = async (data, containers, index) => {
	const container = containers[index];

	resultData[data.id][index] = await parseReview(container, index);

	if (index < containers.length - 1) return await parse(data, containers, index + 1);
};